var gulp 			= require('gulp');
var jshint 			= require('gulp-jshint');
var uglify 			= require('gulp-uglify');
var concat 			= require('gulp-concat');
var sass 			= require('gulp-sass');
var rename 			= require('gulp-rename');
var sourcemaps 		= require('gulp-sourcemaps');
var browserSync 	= require('browser-sync').create();


// ////////////////////////////////////////////////
// Browser-Sync Tasks
// // /////////////////////////////////////////////
gulp.task('browserSync', function() {
    browserSync.init({
        proxy: "http://localhost/majestic-technical-test/"
    });
});

// ///////////////////////////////////////////////
// HTML Task
// ///////////////////////////////////////////////
gulp.task('html', function(){
    gulp.src('./*.html')
    .pipe(browserSync.stream());
});

// ///////////////////////////////////////////////
// SASS Task
// ///////////////////////////////////////////////
gulp.task('sass', function () {
    gulp.src('./css/sass/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dest'))
		.pipe(rename('all.min.css'))
		.pipe(gulp.dest('./dest'))
		.pipe(browserSync.stream());
});

// ////////////////////////////////////////////////
// JS Lint for errors
// // /////////////////////////////////////////////
gulp.task('lint', function() {
	return gulp.src('./js/*.js')
		.pipe(jshint.reporter('default'));
});

// ////////////////////////////////////////////////
// Handle errors
// // /////////////////////////////////////////////
function handleError(err) {
  	console.log(err.toString());
  	this.emit('end');
}

// ////////////////////////////////////////////////
// Load the scripts
// // /////////////////////////////////////////////
gulp.task('scripts', function() {

	return gulp.src(
			[
				'./js/jquery-1.11.1.min.js',
				'./js/jquery.validate.min.js',
				'./js/custom.js'
			]
		)
		.pipe(concat('all.js'))
		.pipe(gulp.dest('./dest'))
		.pipe(rename('all.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./dest'))
		.pipe(browserSync.stream());

});

// ////////////////////////////////////////////////
// Watch task to look for changes
// // /////////////////////////////////////////////
gulp.task('watch', function(){
	gulp.watch('./css/sass/*.scss', ['sass']);
	gulp.watch('./js/*.js', ['scripts']);	
    gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('default', ['watch', 'browserSync', 'sass', 'scripts']);
