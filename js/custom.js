(function(){
	"use strict";

	
	//validate the form
	function leave_comment_validation(form){

		var formParentUl 	= form.closest('ul'),
			child_form_li	= formParentUl.find('>.form_li'),
			textarea_val 	= form.find('textarea').val(); 

		var validateComment = form.validate({
			ignore: [],
			rules: {
				comment: {
					required: true,				
					minlength: 10
				}
			},
			messages: {
				comment: {
					required: "Mandatory",
					minlength: "Minim 10 caractere"
				}
			},	
			submitHandler: function(form) {
				//var textarea_val 	= form.find('textarea').val();

				var new_comment_val = $(form).find('textarea[name="comment"]').val();

				var new_comment = '<li><div class="review_header"><div class="review_header_author">Anonim</div><div class="review_header_time">Just now</div></div><div class="review_content">'+ new_comment_val +'</div><div class="review_footer"><button name="like" value="Like" type="submit">Like</button><div class="review_footer_like_count small_text"><i>0</i> likes</div></div></li>'
				
				//append the new comment to the parent ul but before the form to answer
				child_form_li.before(new_comment);

				//reset the value of textarea
				$(form).find('textarea[name="comment"]').val('');

				//call the likes function here
				call_like_functionality();
			} 
		});
	};

	//count and print likes on button click
	function like_functionality(button){
		var likes	= parseInt(button.closest('.review_footer').find('i').text());

		button.on('click', function(){
			if($(this).hasClass('pink')){
				if($(this).closest('.review_footer').find('.error').length){
					//do nothing. the error is printed
				}else{
					$(this).closest('.review_footer').append('<div class="error">You already liked this</div>');
				}
			}else{
				$(this).addClass('pink');
				console.log( likes );
				if(likes == 0){
					$(this).closest('.review_footer').find('.review_footer_like_count').text("Thank you! This is the first like");
				}
				else {
					$(this).closest('.review_footer').find('.review_footer_like_count').text("You and "+(likes)+" other people like this");
				}
			}
		});
	};

	//call the like funtionality
	function call_like_functionality(){
		//for each form validate on submit
		$('button[name="like"]').each(function(){
			like_functionality($(this));
		});
	};

	$(document).ready(function(){

		//for each form validate on submit
		$('.comment_form').each(function(){
			leave_comment_validation($(this));
		});

		call_like_functionality();
	});

})();